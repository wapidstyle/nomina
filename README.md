# Nomina
Make it easy.
## Installation
Just use Doorstep:
```shell
dinstall wapidstyle/nomina
```
## Usage
Nomina is *very* easy to use. :smiley:

The configuration is written in JSON, making it quite simple.
### Main Page
Navigate to the Nomina directory. This, by default, would be in Nomina on your desktop.  
Next, go to `config/main_page.json`.
You should see something like this:
```json
"header-1":"COMPANY/USER NAME"
"header-2":"website tagline"
...
```
Here's the setup:
> ###### Headers
> The headers are configured through the first few lines.  
> `"header-1"`:"String to identify the big header."

TODO
